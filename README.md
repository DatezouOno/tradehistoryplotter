# README #

これはGMOのFXPlusのトレード記録をMT4のチャート上にプロットするインジケーターです。

### What is this repository for? ###

* TradeHistoryPlotter
* Latest Version is 2.1 

### How do I get set up? ###

* MetaEditorのデーターフォルダー（C:\Users\<ユーザー名>\AppData\Roaming\MetaQuotes\Terminal\<ID>\MQL4\Indicators）配下に TradeHistoryPlotter.mq4 を置き、コンパイルするとMT4から使えるようになります。

### Contribution guidelines ###

* まずはブランチを切ってください。あとでマージを考えようかと。

### Who do I talk to? ###

* datezou@gmail.comまで