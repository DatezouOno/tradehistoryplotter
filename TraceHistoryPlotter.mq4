//+------------------------------------------------------------------+
//|                                          TraceHistoryPlotter.mq4 |
//|                                       Copyright 2014, DatezouOno |
//|                                      http://datezou.blog.fc2.com |
//+------------------------------------------------------------------+
#property copyright  "Copyright 2014, DatezouOno"
#property link       "http://datezou.blog.fc2.com"
#property version    "2.1"
#property indicator_chart_window
extern string FileName           = "FXNeo.csv";
extern string Delimiter          = ",";
extern double LocalGMT           = 9;
extern bool   LocalSummerTime    = false;
extern bool   LocalUS_SummerTime = false;
extern double ServerGMT          = 4;
extern bool   ServerSummerTime   = false;
extern bool   ServerUS_SummerTime= false;
extern int    LineWidth          = 1;
extern int    EntryArrowStyle    = 5;
extern int    ExitArrowStyle     = 5;
extern int    ArrowSize          = 1;
int           LineState;
extern string CurrencyPair       = "USD/JPY";
extern int    InitialWorkSize    = 256;
extern string StrBuy             = "買";
extern string StrSell            = "売";
extern string StrEntry           = "FXネオ新規";
extern string StrExit            = "FXネオ決済";
extern int NumColumns            = 48;
int err;
//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
   int Handle;
   int i,n;
   string TradeNumber,TempStr;
   double OrderPrice,OpenPrice,ClosePrice;
   int OrderVolume;
   string OrderTime,OpenTime,CloseTime;
   string OrderCategory;
   datetime OpenDateTime,CloseDateTime;
   string BufOdrTstamp[];
   double BufOdrPrice[];
   int BufOdrVolume[];
   int CurPos = 0;
   int WorkSize = InitialWorkSize;
   int Index;
   bool BuyFlag;
   bool TempFlag = false;
   string EntryArrow,ExitArrow;

   // CSV Header: 約定日時	取引区分	受渡日	約定番号	銘柄名	銘柄コード	限月	コールプット区分	権利行使価格	権利行使価格通貨	カバードワラント商品種別	売買区分	通貨	市場	口座	信用区分	約定数量	約定単価	コンバージョンレート	手数料	手数料消費税	建単価	新規手数料	新規手数料消費税	管理費	名義書換料	金利	貸株料	品貸料	前日分値洗	経過利子（円貨）	経過利子（外貨）	経過日数（外債）	所得税（外債）	地方税（外債）	金利・価格調整額（CFD）	配当金調整額（CFD）	売建単価（くりっく365）	買建単価（くりっく365）	円貨スワップ損益	外貨スワップ損益	約定金額（円貨）	約定金額（外貨）	決済金額（円貨）	決済金額（外貨）	実現損益	受渡金額（円貨）	備考
   
//----

   ArrayResize(BufOdrTstamp,WorkSize);
   ArrayResize(BufOdrPrice,WorkSize);
   ArrayResize(BufOdrVolume,WorkSize);
   // Initialize BufOdrTstamp as we traverse through this array to find open position.
   for(n=0;n<WorkSize;n++) BufOdrTstamp[n] = "";

   Handle = FileOpen(FileName,FILE_CSV|FILE_READ,Delimiter);

   if(Handle !=INVALID_HANDLE) {
      // Skip the first line.
      // When reading from a csv-file, FileReadString will read from the current position till the nearest delimiter or till the text string end character.
      for(n=0;n<NumColumns;n++) {
         TempStr = FileReadString(Handle);
         //Print("Header:",n,TempStr);
      }
      i = 0;

      while(FileIsEnding(Handle) != true) {
         TradeNumber    = "#" + i;

         OrderTime      = FileReadString(Handle); // 約定日時
         StringReplace(OrderTime,"\"","");
         //Print("TradeNumber:",TradeNumber," 約定日時:",OrderTime);
         OrderCategory  = FileReadString(Handle); // 取引区分
         StringReplace(OrderCategory,"\"","");
         //Print("TradeNumber:",TradeNumber," 取引区分:",OrderCategory);

         if((OrderCategory != StrEntry) && (OrderCategory != StrExit)) {
             // This isn't open nor close.
             // Move to the end of line.
             for(n=0;n<NumColumns-2;n++) {
             TempStr = FileReadString(Handle);
             //Print("Read to end of line:",TempStr);
             }
             continue;
         }

         TempStr     = FileReadString(Handle); // 受渡日
         //Print("TradeNumber:",TradeNumber," 受渡日: ", TempStr);
         TempStr     = FileReadString(Handle); // 約定番号
         //Print("TradeNumber:",TradeNumber," 約定番号: ",TempStr);
         TempStr     = FileReadString(Handle); // 銘柄名
         StringReplace(TempStr,"\"","");
         //Print("TradeNumber:",TradeNumber," 銘柄名: ",TempStr);
         
         // Ignore the record if it's currency pair was not CurrencyPair.
         if(TempStr != CurrencyPair) {
            //Print("CurrencyPair:",TempStr);
            // Go to the next line.
            for(n=0;n<NumColumns-5;n++) TempStr = FileReadString(Handle);
            continue;
         }
         
         TempStr     = FileReadString(Handle); // 銘柄コード
         //Print("TradeNumber:",TradeNumber," 銘柄コード: ",TempStr);
         TempStr     = FileReadString(Handle); // 限月
         //Print("TradeNumber:",TradeNumber," 限月: ",TempStr);
         TempStr     = FileReadString(Handle); // コールプット区分
         //Print("TradeNumber:",TradeNumber," コールプット区分: ",TempStr);
         TempStr     = FileReadString(Handle); // 権利行使価格
         //Print("TradeNumber:",TradeNumber," 権利行使価格: ",TempStr);
         TempStr     = FileReadString(Handle); // 権利行使価格通貨
         //Print("TradeNumber:",TradeNumber," 権利行使価格通貨: ",TempStr);
         TempStr     = FileReadString(Handle); // カバードワラント商品種別
         //Print("TradeNumber:",TradeNumber," カバードワラント商品種別: ",TempStr);

         TempStr     = FileReadString(Handle); //売買区分
         StringReplace(TempStr,"\"","");
         //Print("TradeNumber:",TradeNumber," 売買区分: ",TempStr);

         if(TempStr == StrBuy) {
            BuyFlag = true;
         } else if(TempStr == StrSell) {
            BuyFlag = false;
         }

         TempStr     = FileReadString(Handle); //通貨
         //Print("TradeNumber:",TradeNumber," 通貨: ",TempStr);
         TempStr     = FileReadString(Handle); //市場
         //Print("TradeNumber:",TradeNumber," 市場: ",TempStr);
         TempStr     = FileReadString(Handle); //口座
         //Print("TradeNumber:",TradeNumber," 口座: ",TempStr);
         TempStr     = FileReadString(Handle); //信用区分
         //Print("TradeNumber:",TradeNumber," 信用区分: ",TempStr);
         TempStr     = FileReadString(Handle);
         StringReplace(TempStr,"\"","");
         OrderVolume = StrToInteger(TempStr); //約定数量
         //Print("TradeNumber:",TradeNumber," 約定数量: ",OrderVolume);
         TempStr     = FileReadString(Handle);
         StringReplace(TempStr,"\"","");
         OrderPrice  = StrToDouble(TempStr); //約定単価
         //Print("TradeNumber:",TradeNumber," 約定単価: ",OrderPrice);
         TempStr     = FileReadString(Handle); //コンバージョンレート
         //Print("TradeNumber:",TradeNumber," コンバージョンレート: ",TempStr);
         TempStr     = FileReadString(Handle); //手数料
         //Print("TradeNumber:",TradeNumber," 手数料: ",TempStr);
         TempStr     = FileReadString(Handle); //手数料消費税
         //Print("TradeNumber:",TradeNumber," 手数料消費税: ",TempStr);
         TempStr     = FileReadString(Handle);
         StringReplace(TempStr,"\"","");
         OpenPrice   = StrToDouble(TempStr); //建単価
         //Print("TradeNumber:",TradeNumber," 建単価: ",OpenPrice);

         // Read through the rest of the line.
         for(n=0;n<NumColumns-22;n++) TempStr = FileReadString(Handle);

         // This is a new position.
         if(OrderCategory == StrEntry) {
            // Find unused slot and save the order information.
            TempFlag = false;
            for(n=0;n<WorkSize;n++) {
               if(BufOdrTstamp[CurPos] == "") {
                  BufOdrTstamp[CurPos] = OrderTime;
                  BufOdrPrice[CurPos] = OrderPrice;
                  BufOdrVolume[CurPos] = OrderVolume;
                  CurPos++;
                  TempFlag = true;
                  break; // Exit for loop.
               }
               CurPos++;
               // Return to the beginning of the array.
               if(CurPos == WorkSize) CurPos = 0;
            }
            // If we didn't find any room then extend the array,
            // and save the order information.
            if(TempFlag == false) {
               ArrayResize(BufOdrTstamp,WorkSize+10);
               ArrayResize(BufOdrPrice,WorkSize+10);
               ArrayResize(BufOdrVolume,WorkSize+10);
               // Initialize BufOdrTstamp as we traverse through this array to find open position.
               for(n=WorkSize;n<WorkSize+10;n++) BufOdrTstamp[n] = "";
               CurPos = WorkSize;
               WorkSize += 10;
               BufOdrTstamp[CurPos]   = OrderTime;
               BufOdrPrice[CurPos]   = OrderPrice;
               BufOdrVolume[CurPos] = OrderVolume;
               CurPos++;
            }
            //Print("Entry: BuyFlag",BuyFlag," CurPos=",CurPos," OrderTime=",OrderTime," OrderPrice=",OrderPrice," OrderVolume=",OrderVolume);
            continue; // Return to the beginning of the while loop.
            
         // This is a liquidation.
         } else if(OrderCategory == StrExit) {
         //Print("Exit : BuyFlag",BuyFlag," CurPos=",CurPos," OrderTime=",OrderTime," OrderPrice=",OrderPrice," OrderVolume=",OrderVolume," OpenPrice=",OpenPrice);
            // Search for the time when the position was created.
            TempFlag = false;
            for(n=0;n<WorkSize;n++) {
               Index = ((CurPos + WorkSize - n) % WorkSize);
               //Print("BufOdrPrice[",Index,"]=",BufOdrPrice[Index]," OpenPrice=",OpenPrice);
               if(BufOdrPrice[Index] == OpenPrice) {
                  //Print("OpenTime = BufOdrTstamp[",Index,"]=",BufOdrTstamp[Index]);
                  OpenTime = BufOdrTstamp[Index];
                  // Subtract liquidated volume from current position.
                  if(BufOdrVolume[Index] > OrderVolume) {
                     BufOdrVolume[Index] -= OrderVolume;
                  } else {
                     // Set the element unused.
                     BufOdrTstamp[Index] = "";
                     BufOdrPrice[Index] = 0;
                     BufOdrVolume[Index] = 0;
                  }
                  TempFlag = true;                
                  break; // Exit for loop.
               }
            }

            // If we couldn't find any match, discard this record.
            // (We assume that new position is created prior to it's liquidation in the CSV file.) 
            if(TempFlag == false) {
               continue; // Return to the beginning of the while loop.
            }


         } else {
            // This is not a new nor close order.
               continue; // Return to the beginning of the while loop.   
         }
         
         // We are here because this was a liquidation record.

         CloseTime      = OrderTime;
         ClosePrice     = OrderPrice;
         //Print("ClosePrice: ",IntegerToString(ClosePrice)," CloseTime: ",CloseTime);

         OpenDateTime   = TimeOffset(OpenTime,LocalGMT,LocalSummerTime,LocalUS_SummerTime,ServerGMT,ServerSummerTime,ServerUS_SummerTime);
         CloseDateTime  = TimeOffset(CloseTime,LocalGMT,LocalSummerTime,LocalUS_SummerTime,ServerGMT,ServerSummerTime,ServerUS_SummerTime);
         //Print("OpenTime: ",OpenTime," OpenPrice: ",OpenPrice," CloseTime: ",CloseTime," ClosePrice: ",ClosePrice);

         EntryArrow     = TradeNumber + "_Entry";
         ExitArrow      = TradeNumber + "_Exit";

         ObjectCreate(TradeNumber,OBJ_TREND,0,OpenDateTime,OpenPrice,CloseDateTime,ClosePrice);
         ObjectSet(TradeNumber,OBJPROP_COLOR,White);
         ObjectSet(TradeNumber,OBJPROP_WIDTH,LineWidth);
         ObjectSet(TradeNumber,OBJPROP_STYLE,STYLE_DOT);
         ObjectSet(TradeNumber,OBJPROP_RAY,0);

         ObjectCreate(EntryArrow,OBJ_ARROW_LEFT_PRICE,0,OpenDateTime,OpenPrice);
         if(BuyFlag) {
            ObjectSet(EntryArrow,OBJPROP_COLOR,DeepSkyBlue);
         } else {
            ObjectSet(EntryArrow,OBJPROP_COLOR,Red);
         }
         ObjectSet(EntryArrow,OBJPROP_WIDTH,ArrowSize);
         //ObjectSet(EntryArrow,OBJPROP_ARROWCODE,EntryArrowStyle);

         ObjectCreate(ExitArrow,OBJ_ARROW_RIGHT_PRICE,0,CloseDateTime,ClosePrice);
         ObjectSet(ExitArrow,OBJPROP_COLOR,White);
         ObjectSet(ExitArrow,OBJPROP_WIDTH,ArrowSize);
         //ObjectSet(ExitArrow,OBJPROP_ARROWCODE,ExitArrowStyle);
         i++;
      }
      FileClose(Handle);
   } else {
      err = GetLastError();
      Print("Operation FileOpen failed, error ",err); 
      if(err == ERR_FILE_CANNOT_OPEN) Print("File \"",FileName,"\" is opened by other program.");

   }
//----
   LineState = i;
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
   string StrName;
//----
   for(int i = 0 ; i <= LineState ; i++) {
      StrName = "#" + i;
      ObjectDelete(StrName);
      StrName = "#" + i + "_Entry";
      ObjectDelete(StrName);
      StrName = "#" + i + "_Exit";
      ObjectDelete(StrName);
   }
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
//----
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Time Offet function                                              |
//+------------------------------------------------------------------+
datetime TimeOffset(string strTime , double localTimeToGMT , bool useSummerTime , bool useUS_SummerTime , double serverGMT , bool serverSummerTime , bool serverUS_SummerTime) {
//----
   int DataTime;
   StringReplace(strTime,"\"","");
   StringReplace(strTime,"/",".");
   DataTime = StrToTime(strTime);
   if(LocalSummerTime) {
      if(LocalUS_SummerTime && NYSummerTime(DataTime)) {
         DataTime = DataTime - (LocalGMT + 1) * 3600;
      } else if(LocalUS_SummerTime == false && LondonSummerTime(DataTime)) {
         DataTime = DataTime - (LocalGMT + 1) * 3600;
      } else {
         DataTime = DataTime - LocalGMT * 3600;
      }
   } else {
         DataTime = DataTime - LocalGMT * 3600;
   }

   if(ServerSummerTime) {
      if(ServerUS_SummerTime && NYSummerTime(DataTime)) {
         DataTime = DataTime + (LocalGMT - 1) * 3600;
      } else if(ServerUS_SummerTime == false && LondonSummerTime(DataTime)) {
         DataTime = DataTime + (LocalGMT - 1) * 3600;
      } else {
         DataTime = DataTime + LocalGMT * 3600;
      }
   } else {
         DataTime = DataTime + LocalGMT * 3600;
   }
//----
   return(DataTime);
}
//+------------------------------------------------------------------+
//| LondonSummerTime                                                 |
//+------------------------------------------------------------------+
bool LondonSummerTime(datetime TimeData)
  {
   int LeapYear;
   datetime SummerStart,SummerEnd;
   
   LeapYear = (TimeYear(TimeData) - 1968) / 4;
   SummerStart = ((TimeYear(TimeData) - 1970) * 365 + LeapYear + 90) * 86400;
   SummerStart = SummerStart - TimeDayOfWeek(SummerStart) * 86400;
   SummerEnd = ((TimeYear(TimeData) - 1970) * 365 + LeapYear + 304) * 86400;
   SummerEnd = SummerEnd - TimeDayOfWeek(SummerEnd) * 86400;
   
   if(SummerStart < TimeData && TimeData < SummerEnd) return(true);
   else return(false);
   
  }
//+------------------------------------------------------------------+
//| NYSummerTime                                                     |
//+------------------------------------------------------------------+
bool NYSummerTime(datetime TimeData)
  {
   int LeapYear;
   datetime SummerStart,SummerEnd;
   
   LeapYear = (TimeYear(TimeData) - 1968) / 4;
   SummerStart = ((TimeYear(TimeData) - 1970) * 365 + LeapYear + 59) * 86400;
   SummerStart = SummerStart + ((7 - TimeDayOfWeek(SummerStart)) % 7 + 7) * 86400;
   SummerEnd = ((TimeYear(TimeData) - 1970) * 365 + LeapYear + 304) * 86400;
   SummerEnd = SummerEnd + ((7 - TimeDayOfWeek(SummerEnd)) % 7) * 86400;
   
   if(SummerStart < TimeData && TimeData < SummerEnd) return(true);
   else return(false);
  }

